provider "dyn" {
  customer_name = var.customer_name
  username = var.dyn_user 
  password = var.dyn_password
}

resource "dyn_record" "tftest_a" {
  zone = "test2.int.nsidc.org"
  name = "vmterraformtest"
  value = "172.18.225.250"
  type = "A"
  ttl = "21600"
}

resource "dyn_record" "tftest_cname" {
  zone = "test2.int.nsidc.org"
  name = "dev.vmterraformtest.mlaxer"
  value = "vmterraformtest.test2.int.nsidc.org."
  type = "CNAME"
  ttl = "86400"
}
